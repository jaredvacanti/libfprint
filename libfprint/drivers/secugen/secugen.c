/*
 * SecuGen USB U20 driver for libfprint
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#define FP_COMPONENT "secugen"

#include <stdio.h>
#include <fp-image.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include "secugen.h"

G_DEFINE_TYPE (FpiDeviceSecugen,
               fpi_device_secugen,
               FP_TYPE_IMAGE_DEVICE);

static void
dev_activate (FpImageDevice *dev)
{
  FpiDeviceSecugen *self = FPI_DEVICE_SECUGEN (dev);

  fpi_image_device_activate_complete (dev, NULL);

}

static void
dev_deactivate (FpImageDevice *dev)
{
  FpiDeviceSecugen *self = FPI_DEVICE_SECUGEN (dev);

  SGFPM_SetLedOn (self->hFPM, FALSE); // just to be sure

  fpi_image_device_deactivate_complete (dev, NULL);
}


static void
dev_init (FpImageDevice *dev)
{

  FpiDeviceSecugen *self = FPI_DEVICE_SECUGEN (dev);

  // Create a SGFPM object
  DWORD rc = SGFPM_Create (&self->hFPM);

  if (rc != SGFDX_ERROR_NONE)
    fp_info ("ERR: SGFPM_Create returns %ln", &rc);

  SGFPM_Init (self->hFPM, SG_DEV_FDU05); // use SG_DEV_AUTO for unknown dev

  // Open the fingerprint reader
  rc = SGFPM_OpenDevice (self->hFPM, 0);

  if (rc != SGFDX_ERROR_NONE)
    fp_info ("ERR: SGFPM_OpenDevice returns %ln", &rc);

  rc = SGFPM_GetDeviceInfo (self->hFPM, &self->device_info);

  if (rc == SGFDX_ERROR_NONE)
    {
      fp_info ("device_info.DeviceID   : %ld", self->device_info.DeviceID);
      fp_info ("device_info.DeviceSN   : %s",  self->device_info.DeviceSN);
      fp_info ("device_info.ComPort    : %ld", self->device_info.ComPort);
      fp_info ("device_info.ComSpeed   : %ld", self->device_info.ComSpeed);
      fp_info ("device_info.ImageWidth : %ld", self->device_info.ImageWidth);
      fp_info ("device_info.ImageHeight: %ld", self->device_info.ImageHeight);
      fp_info ("device_info.Contrast   : %ld", self->device_info.Contrast);
      fp_info ("device_info.Brightness : %ld", self->device_info.Brightness);
      fp_info ("device_info.Gain       : %ld", self->device_info.Gain);
      fp_info ("device_info.ImageDPI   : %ld", self->device_info.ImageDPI);
      fp_info ("device_info.FWVersion  : %04X",
               (unsigned int) self->device_info.FWVersion);
    }
  else
    {
      fp_info ("GetDeviceInfo: Failed : ErrorCode = %ld", rc);
    }


  fpi_image_device_open_complete (dev, NULL);
}


static void
dev_deinit (FpImageDevice *dev)
{

  FpiDeviceSecugen *self = FPI_DEVICE_SECUGEN (dev);

  SGFPM_SetLedOn (self->hFPM, FALSE); // just to be sure
  SGFPM_CloseDevice (self->hFPM);
  SGFPM_Terminate (self->hFPM);
  self->hFPM = NULL;

  fpi_image_device_close_complete (dev, NULL);
}


static void
dev_change_state (FpImageDevice *dev, FpiImageDeviceState state)
{
  FpiDeviceSecugen *self = FPI_DEVICE_SECUGEN (dev);

  // A usual run would look like:
  // inactive -> activating: activate vfunc is called
  // activating -> idle: fpi_image_device_activate_complete()
  // idle -> await-finger-on
  // await-finger-on -> capture: fpi_image_device_report_finger_status()
  // capture -> await-finger-off: fpi_image_device_image_captured()
  // await-finger-off -> idle: fpi_image_device_report_finger_status()
  // idle -> deactivating: deactivate vfunc is called
  // deactivating -> inactive: fpi_image_device_deactivate_complete()

  DWORD result;

  switch (state)
    {

    case FPI_IMAGE_DEVICE_STATE_INACTIVE:
      break;

    case FPI_IMAGE_DEVICE_STATE_ACTIVATING:
      break;

    case FPI_IMAGE_DEVICE_STATE_DEACTIVATING:
      break;

    case FPI_IMAGE_DEVICE_STATE_IDLE:
      break;

    case FPI_IMAGE_DEVICE_STATE_AWAIT_FINGER_ON:

      // use file-to-key (ftok) to create a message queue
      // receive notifications when a finger is present on
      // the reader, when in "auto-detect" mode, or "await-finger-on" state
      self->key = ftok (".", 'a'); // 'a' is an arbitrary seed value
      self->msg_qid = msgget (self->key, IPC_CREAT | 0660);

      fp_info ("Message Queue ID is : %d", self->msg_qid);

      SGFPM_SetLedOn (self->hFPM, TRUE);
      result = SGFPM_EnableAutoOnEvent (self->hFPM, TRUE, &self->msg_qid, NULL);

      while (1)
        {
          int fingerPresent = 0;

          msgrcv (self->msg_qid, &self->qbuf, MAX_SEND_SIZE, FDU05_MSG, 0);

          // fp_info ("Type: %ld Text: %s", self->qbuf.mtype, self->qbuf.mtext);
          if (strlen (self->qbuf.mtext) > 0)
            fingerPresent = atol (self->qbuf.mtext);

          if (fingerPresent)
            {

              result = SGFPM_EnableAutoOnEvent (self->hFPM, FALSE, &self->msg_qid, NULL);

              if (result != SGFDX_ERROR_NONE)
                {
                  fp_info ("STOP AUTO ON FAIL - [%ld]", result);
                  break;
                }
              msgctl (self->msg_qid, IPC_RMID, 0);

              SGFPM_SetLedOn (self->hFPM, FALSE);
              fpi_image_device_report_finger_status (dev, FP_FINGER_STATUS_PRESENT);
              break;
            }
        }

      break;

    case FPI_IMAGE_DEVICE_STATE_CAPTURE:

      DWORD err;
      BYTE *CurrentImageBuffer;
      int size = self->device_info.ImageWidth * self->device_info.ImageHeight;
      CurrentImageBuffer = malloc (size);
      DWORD GET_IMAGE_TIMEOUT = 10000;   // 10 seconds
      DWORD GET_IMAGE_DESIRED_QUALITY = 50;


      err = SGFPM_GetImageEx (self->hFPM,
                              CurrentImageBuffer,
                              GET_IMAGE_TIMEOUT,
                              NULL,
                              GET_IMAGE_DESIRED_QUALITY);

      if (err != SGFDX_ERROR_NONE)
        {
          fp_info ("GetImage: Failed : ErrorCode = %ld", err);
          fpi_image_device_retry_scan (dev, FP_DEVICE_RETRY_GENERAL);
        }

      FpImage *img = fp_image_new (self->device_info.ImageWidth,
                                   self->device_info.ImageHeight);
      img->flags |= FPI_IMAGE_PARTIAL;
      memcpy (img->data, CurrentImageBuffer, size);
      fpi_image_device_image_captured (dev, img);

      break;

    case FPI_IMAGE_DEVICE_STATE_AWAIT_FINGER_OFF:
      sleep (1);
      fpi_image_device_report_finger_status (dev, FP_FINGER_STATUS_NONE);
      break;

    default:
      break;
    }
}

/* Usb id table of device */
static const FpIdEntry id_table[] = {
  {
    .vid = 0x1162,
    .pid = 0x2200,
  },
};


static void
fpi_device_secugen_init (FpiDeviceSecugen *self)
{
  self->hFPM = NULL;
  self->qbuf.mtype = FDU05_MSG;
}


static void
fpi_device_secugen_class_init (FpiDeviceSecugenClass *klass)
{
  FpDeviceClass *dev_class = FP_DEVICE_CLASS (klass);
  FpImageDeviceClass *img_class = FP_IMAGE_DEVICE_CLASS (klass);

  dev_class->id = "secugen";
  dev_class->full_name = "SecuGen Fingerprint Hamster Pro 20";
  dev_class->type = FP_DEVICE_TYPE_USB;
  dev_class->id_table = id_table;
  dev_class->scan_type = FP_SCAN_TYPE_PRESS;

  img_class->img_open = dev_init;
  img_class->img_close = dev_deinit;
  img_class->change_state = dev_change_state;
  img_class->activate = dev_activate;
  img_class->deactivate = dev_deactivate;

  img_class->img_width = 300;
  img_class->img_height = 400;
}

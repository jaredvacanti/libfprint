/*
 * Secugen Corp. SecuGen USB U20 driver for libfprint
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <glib-object.h>
#include "drivers_api.h"
// #include <sys/msg.h>
#include "sgfplib.h"

// A convenience macro for emitting the usual declarations in the header file
G_DECLARE_FINAL_TYPE (FpiDeviceSecugen,
                      fpi_device_secugen,
                      FPI,
                      DEVICE_SECUGEN,
                      FpImageDevice);

struct _FpiDeviceSecugen
{
  FpImageDevice     parent;
  HSGFPM            hFPM;
  SGDeviceInfoParam device_info;
  int               msg_qid;
  key_t             key;
  struct msgbuf     qbuf;
};
